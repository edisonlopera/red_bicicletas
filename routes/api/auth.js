var express = require('express');
var router = express.Router();
var passport = require('../../config/passport');
var authControllerAPI = require('../../controllers/api/authControllerAPI');

router.post('/authenticate',authControllerAPI.authenticate);
router.post('/forgotPassword',authControllerAPI.forgotPassword);
router.post('/facebook_token',passport.authenticate('facebook-token'),authControllerAPI.authFacebookToken);

module.exports = router;