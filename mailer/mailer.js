"use strict";
const nodemailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid-transport");


// async..await is not allowed in global scope, must use a wrapper
async function main(mailOptions) {

  let mailConfig;
  if(process.env.NODE_ENV==='production'){
    const options = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET
      }
    }
    mailConfig = sgTransport(options);
  }else{
    if(process.env.NODE_ENV==='staging'){
      console.log('xxxxxxxxxxxxxx');
      const options = {
        auth: {
          api_key: process.env.SENDGRID_API_SECRET
        }
      }
      mailConfig = sgTransport(options);
    }else{
      mailConfig = {
          host: 'smtp.ethereal.email',
          port: 587,
          Security : 'STARTTLS',
          SMTPAuth : true,
          SMTPSecure : 'tls',
          auth: {
            user: process.env.ethereal_user,
            pass: process.env.ethereal_pwd
        }
      }
    }
  }

  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport(mailConfig);

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: process.env.ethereal_from,
    to: mailOptions.to,
    subject: mailOptions.subject,
    html: mailOptions.text,
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

exports.sendEmail = (mailOptions) => {
    console.log('-- enviando email ...');
    main(mailOptions).catch(console.error);
}
