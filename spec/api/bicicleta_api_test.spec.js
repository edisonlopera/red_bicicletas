var Bicicleta = require('../../models/bicicletas');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

const uriserver='http://localhost';
const serverPort = ':3000';
const OK = 200;
const CLEAR = 204;
const uri_get_bicicletas = uriserver+serverPort+'/api/bicicletas';
const uri_post_bicicletas = uriserver+serverPort+'/api/bicicletas/create';
const uri_delete_bicicletas = uriserver+serverPort+'/api/bicicletas/delete';
const uri_update_bicicletas = uriserver+serverPort+'/api/bicicletas/:id/update';

describe('Probando el API de bicicletas',()=>{
    beforeEach(function(done){
        
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,
            {
                useNewUrlParser : true,
                useUnifiedTopology: true
            });
        
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:')); 
        db.once('open', function() {
            console.log('Conectado a la base de datos de test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err)
                console.log(err);
            done();
        });
    });

    describe('GET bicicletas /',()=>{
        it(`Status ${OK}`,(done) => {
            request.get(uri_get_bicicletas,(err,response,body) => {
                if(err)
                    console.log(err);
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(OK);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        })
    });

    
    describe('POST bicicletas /create',()=>{
        it(`Status ${OK}`,(done)=> {

            var headers = {'content-type':'application/json'};
            var aBici = {code:1010,color:'black',modelo:'urbana',lat:-38.676767,lng:-54.989898};

            request.post({
                headers : headers,
                url: uri_post_bicicletas,
                body :  JSON.stringify(aBici)  
            },
            (error,response,body) => {
                if(error)
                    console.info('--error post create bicicletas',error);
                expect(response.statusCode).toBe(OK);
                var result = JSON.parse(body).bicicleta;
                console.log(result);
                expect(result.code).toBe(aBici.code);
                expect(result.color).toBe(aBici.color);
                expect(result.modelo).toBe(aBici.modelo);
                expect(result.ubicacion[0]).toBe(aBici.lat);
                expect(result.ubicacion[1]).toBe(aBici.lng);
                done();
            });
        })
    });
    
});
//OJO ESTA PARTE CORRESPONDE A LA PRIMERA ENTREGA
/*
describe('Probando el API de bicicletas',()=>{
    
    describe('GET bicicletas /',()=>{
        it(`Status ${OK}`,()=> {
            var tam = Bicicleta.allBicis.length;
            expect(Bicicleta.allBicis.length).toBe(tam);
    
            var a = new Bicicleta(1,'negro','chimuela',[-34.6012424,-58.3861497]);
            Bicicleta.add(a);

            request.get(uri_get_bicicletas,(error,response,body) => {
                expect(response.statusCode).toBe(OK);
            });
        })
    });


    describe('POST bicicletas /create',()=>{
        it(`Status ${OK}`,(done)=> {

            var headers = {'content-type':'application/json'};
            var aBici = {'id':1010,'color':'black','modelo':'urbana','lat':-38.676767,'lng':-54.989898};

            request.post({
                headers:headers,
                url: uri_post_bicicletas,
                body :  JSON.stringify(aBici)  
            },
            (error,response,body) => {
                expect(response.statusCode).toBe(OK);
                var result = Bicicleta.findById(aBici.id);
                expect(result.id).toBe(aBici.id);
                expect(result.color).toBe(aBici.color);
                expect(result.modelo).toBe(aBici.modelo);
                expect(result.ubicacion[0]).toBe(aBici.lat);
                expect(result.ubicacion[1]).toBe(aBici.lng);
                done();
            });
        })
    });

    describe('PUT bicicletas /update',()=>{
        it(`Status ${OK}`,(done)=> {

            var headers = {'content-type':'application/json'};
            var aBici = {'id':1212,'color':'black','modelo':'urbana','lat':-38.676767,'lng':-54.989898};

            request.post({
                headers : headers,
                url: uri_post_bicicletas,
                body :  JSON.stringify(aBici)  
            },
            (error,response,body) => {
                expect(response.statusCode).toBe(OK);
                var result = Bicicleta.findById(aBici.id);
                expect(result.id).toBe(aBici.id);
                expect(result.color).toBe(aBici.color);
                expect(result.modelo).toBe(aBici.modelo);
                expect(result.ubicacion[0]).toBe(aBici.lat);
                expect(result.ubicacion[1]).toBe(aBici.lng);
                const lastId = aBici.id;
                aBici.id = 6543;
                aBici.color = 'magenta';
                aBici.modelo = 'todo terreno';
                aBici.lat = -34.980991;
                aBici.lng = -54.879087;
                request.put(
                    {
                        headers : headers,
                        url: uri_update_bicicletas.replace(':id',lastId),
                        body :  JSON.stringify(aBici)  
                    },
                    (errorUpd,respoUpd,bodyUpd)=>{
                        expect(respoUpd.statusCode).toBe(OK);
                        var result = Bicicleta.findById(aBici.id);
                        expect(result.id).toBe(aBici.id);
                        expect(result.color).toBe(aBici.color);
                        expect(result.modelo).toBe(aBici.modelo);
                        expect(result.ubicacion[0]).toBe(aBici.lat);
                        expect(result.ubicacion[1]).toBe(aBici.lng);
                        done();
                    }
                )
                
            });
        })
    });

    describe('DELETE bicicletas /delete',()=>{
        it(`Status ${CLEAR}`,(done)=> {

            var headers = {'content-type':'application/json'};
            var aBici = {'id':212345,'color':'black','modelo':'urbana','lat':-38.676767,'lng':-54.989898};
            var aBiciDel = {'id':aBici.id};
            
            request.post({
                headers : headers,
                url: uri_post_bicicletas,
                body :  JSON.stringify(aBici)  
            },
            (error,response,body) => {

                expect(response.statusCode).toBe(OK);
                var result = Bicicleta.findById(aBici.id);
                expect(result.id).toBe(aBici.id);
                expect(result.color).toBe(aBici.color);
                expect(result.modelo).toBe(aBici.modelo);
                expect(result.ubicacion[0]).toBe(aBici.lat);
                expect(result.ubicacion[1]).toBe(aBici.lng);

                request.delete(
                    {   url : uri_delete_bicicletas,
                        headers : headers,
                        body :  JSON.stringify(aBiciDel)  
                    },
                    (errorDel,responseDel,bodyDel) => {
                        expect(responseDel.statusCode).toBe(CLEAR);
                        var resultDel = undefined;
                        try{
                            resultDel = Bicicleta.findById(aBiciDel.id);
                        }catch(excp1){}
                        expect(resultDel).toBe(undefined);
                        done();
                    })
                    
            });
        })
    });
});
*/