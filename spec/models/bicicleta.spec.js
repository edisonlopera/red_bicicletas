var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletas');


describe('Testing Bicicletas',function(){
    beforeEach(function(done){
        
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,
            {
                useNewUrlParser : true,
                useUnifiedTopology: true
            });
        
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:')); 
        db.once('open', function() {
            console.log('Conectado a la base de datos de test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err)
                console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance',() => {
        it('Crea una instancia de la bicicleta',() => {
            var prueba = {code : 1, color : 'verde' , modelo : 'urbana' , ubicacion : [-34.899098,-54.988067]};
            var bici = Bicicleta.createInstance(prueba.code , prueba.color , prueba.modelo , prueba.ubicacion);
            expect(bici.code).toBe(prueba.code);
        });
    });

    describe('Bicicleta.allBicis',() => {
        it('Comienza vacia',(done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            })
        })
    });
    
    describe('Bicicleta.add',() => {
        it('Agrega únicamente una bici',(done) => {
            var aBici = new Bicicleta({code : 1, color: 'verde', modelo : 'urbana', ubicacion :[-34.878989,-54.989880]});
            Bicicleta.add(aBici,function(err,newBici){
                if(err)
                    console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        })
    });

    describe('Bicicleta.findByCode',() => {
        var codePrue = 12;
        var codePrue2 = 212;
        it(`Debe ingresar y recuperar bici con el código ${codePrue}`,(done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
            });
            
            var aBici = new Bicicleta({code : codePrue, color: 'verde', modelo : 'urbana'});
            Bicicleta.add(aBici,function(err,newBici){
                if(err)
                    console.log(err);
                
                var aBici2 = new Bicicleta({code : codePrue2, color: 'azul', modelo : 'todo terreno'});
                Bicicleta.add(aBici2,function(err,newBici){
                    if(err)
                        console.log(err);
                    Bicicleta.findByCode(codePrue,function(err,targetBici){
                        if(err)
                            console.log(err);
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(aBici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);
                        done();
                    })
                });
            });
        })
    })

    describe('Bicicleta.removeByCode',() => {
        var codePrue = 120;
        var codePrue2 = 345;
        it(`Debe ingresar y borrar la bici con el código ${codePrue}`,(done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
            });
            
            var aBici = new Bicicleta({code : codePrue, color: 'verde', modelo : 'urbana'});
            Bicicleta.add(aBici,function(err,newBici){
                if(err)
                    console.log(err);
                
                var aBici2 = new Bicicleta({code : codePrue2, color: 'azul', modelo : 'todo terreno'});
                Bicicleta.add(aBici2,function(err,newBici){
                    if(err)
                        console.log(err);
                    Bicicleta.findByCode(codePrue,function(err,targetBici){
                        if(err)
                            console.log(err);
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(aBici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);

                        Bicicleta.removeByCode(codePrue2, function(err,reBici){
                            if(err)
                                console.log(err);
                            expect(reBici.n).toBe(1);
                            expect(reBici.ok).toBe(1);
                            expect(reBici.deletedCount).toBe(1);
                            done();
                        });
                        
                    })
                });
            });
        })
    })
});
/*
beforeEach(() => {
    Bicicleta.cleanCollection();
});

describe('Bicicleta.allBicis',()=>{
    it('Comianza vacía',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});


describe('Bicicleta.add',() => {
    if('Agrego una bicileta',()=>{

        var tam = Bicicleta.allBicis.length;
        expect(Bicicleta.allBicis.length).toBe(tam);

        var a = new Bicicleta(1,'rojo','urbana',[-34.6012424,-58.3861497]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(tam+1);
        expect(Bicicleta.allBicis[tam+1]).toBe(a);

    }); 
});


describe('Bicicleta.findById',() => {
    var idPrueba = 1;
    it(`Se inserta y luego se busca una bicicleta por el id ${idPrueba} con los datos ingresados`,()=>{

        var tam = Bicicleta.allBicis.length;
        expect(Bicicleta.allBicis.length).toBe(tam);

        var a = new Bicicleta(idPrueba,'amarillo','montaña',[-34.6012424,-58.3861497]);
        Bicicleta.add(a);
        var aResult = Bicicleta.findById(idPrueba);

        expect(Bicicleta.allBicis.length).toBe(tam+1);
        expect(a.id).toBe(aResult.id);
        expect(a.color).toBe(aResult.color);
        expect(a.modelo).toBe(aResult.modelo);
        expect(a.ubicacion[0]).toBe(aResult.ubicacion[0]);
        expect(a.ubicacion[1]).toBe(aResult.ubicacion[1]);
    }); 
});

describe('Bicicleta.removeById',() => {
    var idPrueba = 1;
    it(`Se inserta, se busca y luego se retira una bicicleta por el id ${idPrueba}`,()=>{

        var tam = Bicicleta.allBicis.length;
        expect(Bicicleta.allBicis.length).toBe(tam);

        var a = new Bicicleta(idPrueba,'amarillo','montaña',[-34.6012424,-58.3861497]);
        Bicicleta.add(a);
        var aResult = Bicicleta.findById(idPrueba);

        expect(Bicicleta.allBicis.length).toBe(tam+1);
        expect(a.id).toBe(aResult.id);
        expect(a.color).toBe(aResult.color);
        expect(a.modelo).toBe(aResult.modelo);
        expect(a.ubicacion[0]).toBe(aResult.ubicacion[0]);
        expect(a.ubicacion[1]).toBe(aResult.ubicacion[1]);

        Bicicleta.removeById(idPrueba);
        var aResult = undefined;
        try{
            aResult = Bicicleta.findById(idPrueba);
        }catch(exp){}
        expect(aResult).toBe(undefined);

    }); 
});*/