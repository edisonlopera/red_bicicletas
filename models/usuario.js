var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var uniqueValidator = require('mongoose-unique-validator');
var Token = require('./token');
var crypto = require('crypto');
var mailer = require('../mailer/mailer');


const saltRounds = 10;
const regex = /^\w+[\.-]*\w+@\w+([\.-]?w+)*(\.\w{2,3})+$/;

const validateEmail = function(email){
    const re = regex;
}
var usuarioSchema = new Schema({
    nombre:{
        type: String,
        trim : true,
        required : [true,'Se requiere el nombre']
    } ,
    email:{
        type: String,
        trim : true,
        required : [true,'Se requiere el correo'],
        lowecase : true,
        unique : true,
        validate : [validateEmail,'El correo no es válido'],
        match : [regex]
    } ,
    observacion:{
        type:String,
        trim : true,
    }
    ,
    password:{
        type: String,
        required : [true,'Se requiere el password']
    } ,     
    passwordResetToken : String,
    passwordResetTokenExpires : Date,
    verificado:{
        type : Boolean,
        default : false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator,{mesage : 'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save',function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password,saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword= function (password){
    return bcrypt.compareSync(password,this.password);
};

usuarioSchema.methods.reservar = function(biciId,desde,hasta,cb){
    var reserva = new Reserva({usuario : this._id , bicicleta : biciId , desde : desde , hasta : hasta});
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){

        if(err){
            return console.log(err.mesage);
        }

        const enlace = 'http://localhost:3000'+'\/token/confirmation\/'+token.token;
        const mail_content = ' \
        <!DOCTYPE html>\
        <html>\
        <head>\
            <meta charset="utf-8">\
            <title>Mi pagina de prueba</title>\
        </head>\
        <body>\
            <h1>Saludos</h1>\
            <p>Para verificar su cuenta por favor haga click en este enlace <a href="'+enlace+'">haga clic a</a></p>\
        </body>\
        </html>';

        const mailOptions={
            from : 'no-replay@redbicicletas.com',
            to : email_destination,
            subject : 'Email de verificación',
            text : mail_content
        }

        mailer.sendEmail(mailOptions,function(err){
            if(err){
                return console.log(err.mesage);
            }
            console.log('Email de verificación enviado a: '     + email_destination);
        });
    });
}

usuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId:this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err)
            return cb(err);
        
        const enlace = 'http://localhost:3000'+'\/resetPassword\/'+token.token;
        
        const mail_content = ' \
            <!DOCTYPE html>\
            <html>\
            <head>\
                <meta charset="utf-8">\
                <title>Reseto de password de cuenta</title>\
            </head>\
            <body>\
                <h1>Saludos</h1>\
                <p>Para resetear su password por favor haga click en este enlace <a href="'+enlace+'">haga clic a</a></p>\
            </body>\
            </html>';
        
        const mailOptions={
            from : 'no-replay@redbicicletas.com',
            to : email_destination,
            subject : 'Reseteo de password de cuenta',
            text : mail_content
        }

        mailer.sendEmail(mailOptions,function(err){
            if(err){
                return console.log(err.mesage);
            }
            console.log('Email de reseteo de cuenta enviado a: '     + email_destination);
        });
        cb(null);
    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(condition);
    self.findOne({
        sort: [
            {'googleId': condition.id},
            {'email': condition.emails[0].value}    
        ]

        },(err,result)=>{
            if(result){
                callback(err,result);
            }else{
                console.log('---------- condition ------------');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = condition._json.sub || condition.emails[0].value+'SIN PASSWORD';
                console.log('---------------- values ---------------');
                console.log(values);
                self.create(values,(err,result)=>{
                    if(err)
                        console.log(err);
                    return callback(err,result);
                });
            }
        }
    );  
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(condition);
    self.findOne({
        sort: [
            {'facebookId': condition.id},
            {'email': condition.emails[0].value}    
        ]

        },(err,result)=>{
            if(result){
                callback(err,result);
            }else{
                console.log('---------- condition ------------');
                console.log(condition);
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');//condition._json.sub || condition.emails[0].value+'SIN PASSWORD';
                console.log('---------------- values ---------------');
                console.log(values);
                self.create(values,(err,result)=>{
                    if(err)
                        console.log(err);
                    return callback(err,result);
                });
            }
        }
    );  
};


module.exports = mongoose.model('Usuario', usuarioSchema);

//Primera versión
/*var usuarioSchema = new Schema({
    nombre : String
    });
    */