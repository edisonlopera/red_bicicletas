var Usuario = require('../../models/usuario');
const OK = 200;

exports.usuarios_list = function(req,res){
    Usuario.find({},function(err,usuarios){
        res.status(OK).json({
            usuarios :usuarios
        });
    });
};

exports.usuarios_create = function(req,res){
    var dataUsuario = {
        nombre : req.body.nombre, 
        email : req.body.email, 
        observacion : req.body.observacion, 
        password :req.body.secreto
    };
    var usuario = new Usuario(dataUsuario);
    usuario.save(function(err){
        res.status(OK).json(usuario);
    });
};

exports.usuario_reservar = function(req, res){
    Usuario.findById(req.body.id, function(err,usuario){
        console.log(usuario);
        var cont = req.body;
        usuario.reservar(cont.bici_id, cont.desde, cont.hasta, function(err){
            console.log('Reserva!!!');
            res.status(OK).send();
        });
    });
};