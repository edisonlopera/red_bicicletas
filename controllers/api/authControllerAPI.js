var Usuario = require('../../models/usuario');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

module.exports = {
    authenticate : function(req,res,next){
        Usuario.findOne({email : req.body.email},function(err,userInfo){
            if(err){
                next(err);
            }else{
                if(userInfo===null){
                    return res.status(401).json({status:'error',message:'Datos de usuario no encontrados',data:null});
                }
                if(userInfo!==null && bcrypt.compareSync(req.body.password, userInfo.password)){
                    
                    const token = jwt.sign({id : userInfo._id} , req.app.get('secretKey') , {expiresIn : '7d'});
                    res.status(200).json({message:'Usuario encontrado', data:{usuario: userInfo, token: token}});
                    
                }else{
                    res.status(401).json({status: 'error' , message : 'Datos de acceso incorrectos' , data: null});
                }
            }
        });
    },
    forgotPassword:function(req,res,next){
        Usuario.findOne({email: req.body.email}, function(err,usuario){
            if(!usuario)
                return res.status(401).json({message : 'Datos de ingreso incorrectos', data: null});
            usuario.resetPassword(function(err){
                if(err)
                    next(err);
                res.status(200).json({message:'Se envió un correo para restablecer datos de acceso', data: null});
            });
        });
    },
    authFacebookToken:function(req,res,next){
        if(req.user){
            req.user.save().then(()=>{
                const token = jwt.sign({id : userInfo._id} , req.app.get('secretKey') , {expiresIn : '7d'});
                res.status(200).json({message:'Usuario encontrado', data:{usuario: userInfo, token: token}})
            }).catch((err)=>{
                console.log(err);
                res.status(500).json({message: err.message});
            });
        }else{
            res.status(401);
        }
    }
};