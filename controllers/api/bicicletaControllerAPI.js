var Bicicleta = require('../../models/bicicletas');

exports.bicicleta_list = function(req,res){
    var respon = {
        bicicletas : []
    };

    try{
        Bicicleta.allBicis(function(err,bicis){
            if(err)
                console.info('--err',err);
            respon.bicicletas = bicis;
            res.status(200).json(respon);
    });
    }catch(exce){
        console.info('--error bicicletaControllerAPI',exce);
    }
}

exports.bicicleta_create = function(req,res){
    console.info('--al inicio de Crear bicicleta',req.body);
    var cont = req.body;
    var bici = new Bicicleta({code:cont.code,color:cont.color,modelo:cont.modelo,ubicacion:[cont.lat,cont.lng]});
    console.info('--Antes de crear bicicleta',bici);
    Bicicleta.add(bici);
    res.status(200).json({bicicleta: bici})
}


exports.bicicleta_delete = function(req,res){
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}

exports.bicicleta_update = function(req,res){
    try{
        var cont = req.body;
        var bici = Bicicleta.findById(req.params.id);
        bici.id = cont.id;
        bici.modelo = cont.modelo;
        bici.color = cont.color;
        bici.ubicacion = [cont.lat,cont.lng];
        res.status(200).json({bicicleta: bici})
    }catch(excp){
        res.status(204).send();
    }
}