var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = { 
    confirmTokenGet : function(req,res,next){
        console.info('--confirmando correo',req.params.token);
        Token.findOne({token : req.params.token},function(err,token){
            if(!token)
                return res.status(400).send({type : 'not-verified' , msg : 'no se ha verificado'});
                //token._id
                //token._userId
            console.info('--data Token',token);
            Usuario.findById(token._userId,function(err,usuario){
                if(!usuario)
                    return res.status(400).send({msg:'No se ha encontrado al usuario solicitado'});
                if(usuario.verificado)
                    return res.redirect('/usaurios');
                usuario.verificado = true;
                usuario.save(function(err){
                    if(err)
                        return res.status(500).send({msg : err.message});
                    res.redirect('/');
                });
            });
        });   
    }
}