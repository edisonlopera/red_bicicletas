var Bicicleta = require('../models/bicicletas');

exports.bicicleta_list = (req,res) =>{ 
    res.render('bicicletas/index',{bicis: Bicicleta.allBicis});
};

exports.bicicleta_create_get = (req,res) =>{ 
    res.render('bicicletas/create');
};

exports.bicicleta_create_post = (req,res) =>{ 
    var cont = req.body;
    var bici = new Bicicleta(cont.id,cont.color,cont.modelo,[cont.lat,cont.lng]);
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
};

exports.bicicleta_delete_post = (req,res) =>{ 
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
};

exports.bicicleta_update_get = (req,res) =>{ 
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update',{bici});
};

exports.bicicleta_update_post = (req,res) =>{ 
    var cont = req.body;
    var bici = Bicicleta.findById(req.params.id);
    bici.id = cont.id;
    bici.modelo = cont.modelo;
    bici.color = cont.color;
    bici.ubicacion = [cont.lat,cont.lng];
    res.redirect('/bicicletas');
};